int notes [] = {262, 294, 330, 349};
#define A 440
#define Bb 466
#define B 494
#define C 523
#define Db 554
#define D 587
#define Eb 622
#define E 659
#define FF 698
#define Gb 740
#define G 784
#define Ab 830
#define AA 880

int noteval = 160;

void setup() 
{
Serial.begin(9600);
}

void loop() 
{
int keyVal = analogRead(A0);
Serial.println(keyVal);

if(keyVal >= 670 && keyVal <= 700){
  tone(8, Gb, noteval);
  delay(noteval);
  tone(8, E, noteval);
  delay(noteval);
  tone(8, Db, noteval);
  delay(noteval);
  tone(8, B, noteval);
  delay(noteval);
  tone(8, Db, noteval);
  delay(noteval);
  tone(8, Gb, noteval*2);
  delay(noteval*2);
  tone(8, E, noteval);
  delay(noteval);
  tone(8, Db, noteval);
  delay(noteval);
  tone(8, E, noteval);
  delay(noteval);
  tone(8, Gb, noteval);
  delay(noteval);
  tone(8, E, noteval);
  delay(noteval);
  tone(8, Db, noteval);
  delay(noteval);
  tone(8, B, noteval);
  delay(noteval);
  tone(8, Db, noteval);
  delay(noteval);
  tone(8, Gb, noteval);
  delay(noteval);
  tone(8, E, noteval*2);
  delay(noteval*2);
}
else if (keyVal >= 550 && keyVal <= 660){
  tone(8, Db);
}
else if (keyVal >= 300 && keyVal <= 400 || keyVal > 730){
  tone(8, B);
}
else if(keyVal >= 3 && keyVal <= 10){
  tone(8, E);
}
else{
  noTone(8);
  }
}

