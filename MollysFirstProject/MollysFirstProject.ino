int delayTime = 500;
int counter = 1;
bool flag = false;

void setup() {
  pinMode(2, OUTPUT); // LED 1 control pin is set up as an output
  pinMode(3, OUTPUT); // same for LED 2 to LED 5
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
}

void loop() {
  digitalWrite(2, HIGH); // Turn LED 1 on
  delay(delayTime / (counter * 2)); // Wait half a second
  digitalWrite(2, LOW); // Turn LED 1 off
  digitalWrite(3, HIGH); // and repeat for LED 2 to LED 5
  delay(delayTime / (counter * 2));
  digitalWrite(3, LOW);
  digitalWrite(4, HIGH);
  delay(delayTime / (counter * 2));
  digitalWrite(4, LOW);
  digitalWrite(5, HIGH);
  delay(delayTime / (counter * 2));
  digitalWrite(5, LOW);
  digitalWrite(6, HIGH);
  delay(delayTime / (counter * 2));
  digitalWrite(6, LOW);
  digitalWrite(5, HIGH);
  delay(delayTime / (counter * 2));
  digitalWrite(5, LOW);
  digitalWrite(4, HIGH);
  delay(delayTime / (counter * 2));
  digitalWrite(4, LOW);
  digitalWrite(3, HIGH);
  delay(delayTime / (counter * 2));
  digitalWrite(3, LOW);

  if (counter >= 10)
    flag = true;
 else if (counter <= 1)
    flag = false;

 if (flag == false)
  counter += 1;
 else
  counter -= 1;
}
