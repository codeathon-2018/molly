#define speaker 2
#define buttonUp 22
#define buttonDown 24

#define noteC4 261.63
#define noteDb4 277.18
#define noteD4 293.67
#define noteEb4 311.13
#define noteE4 329.63
#define noteF4 349.23
#define noteGb4 369.99
#define noteG4 391.99
#define noteAb4 415.31
#define noteA4 440.00
#define noteBb4 466.16
#define noteB4 493.88
#define noteC5 523.25

float octave = 1.00;

void octaveUp()
{
 Serial.println("buttonUp pressed");
 if(octave <= 2)
 octave *= 2;
 while(digitalRead(buttonUp) == HIGH)
 {
 
 }
}

void octaveDown()
{
  Serial.println("buttonDown pressed");
  if(octave >= 1)
  octave /= 2;
  while(digitalRead(buttonDown) == HIGH)
  {
    
  }
}

void setup() 
{
Serial.begin(9600);
}

void loop() 
{
  if(digitalRead(buttonUp) == HIGH)
    octaveUp();
    
  if(digitalRead(buttonDown) == HIGH)
    octaveDown();
  
int keyVal = analogRead(A0);
Serial.println(keyVal);

if (keyVal >= 671 && keyVal <= 680){
  tone(speaker, noteC4 * octave);
}
else if (keyVal >= 666 && keyVal <= 670){
  tone(speaker, noteDb4 * octave);
}
else if(keyVal >= 660 && keyVal <= 664){
  tone(speaker, noteD4 * octave);
}
else if(keyVal >= 640 && keyVal <= 658){
  tone(speaker, noteEb4 * octave);
}
else if(keyVal >= 600 && keyVal <= 620){
  tone(speaker, noteE4 * octave);
}
else if(keyVal >= 550 && keyVal <= 580){
  tone(speaker, noteF4 * octave);
}
else if(keyVal >= 500 && keyVal <= 530){
  tone(speaker, noteGb4 * octave);
}
else if(keyVal >= 420 && keyVal <= 450){
  tone(speaker, noteG4 * octave);
}
else if(keyVal >= 350 && keyVal <= 400){
  tone(speaker, noteAb4 * octave);
}
else if(keyVal >= 300 && keyVal <= 340){
  tone(speaker, noteA4 * octave);
}
else if(keyVal >= 100 && keyVal <= 180){
  tone(speaker, noteBb4 * octave);
}
else if(keyVal >= 30 && keyVal <= 80){
  tone(speaker, noteB4 * octave);
}
else if(keyVal >= 2 && keyVal <= 10){
  tone(speaker, noteC5 * octave);
}
else{
  noTone(speaker);
}
}
