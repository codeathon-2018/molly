#define westButton 3
#define eastButton 12
#define westRed 2
#define westYellow 1
#define westGreen 0
#define eastRed 11
#define eastYellow 10
#define eastGreen 9

#define yellowBlinkTime 500
bool trafficWest = true;
int flowTime = 1000;
int changeDelay = 200;

void setup() {
  pinMode(westButton, INPUT);
  pinMode(eastButton, INPUT);
  pinMode(westRed, OUTPUT);
  pinMode(westYellow, OUTPUT);
  pinMode(westGreen, OUTPUT);
  pinMode(eastRed, OUTPUT);
  pinMode(eastYellow, OUTPUT);
  pinMode(eastGreen, OUTPUT);

  digitalWrite(westRed, LOW);
  digitalWrite(westYellow, LOW);
  digitalWrite(westGreen, HIGH);
  digitalWrite(eastRed, HIGH);
  digitalWrite(eastYellow, LOW);
  digitalWrite(eastGreen, LOW);
}

void loop() 
{
  if ( digitalRead(westButton) == HIGH) // request west>east traffic flow
  {
    if (trafficWest != true)
    {
      trafficWest = true;
      delay(flowTime);
      digitalWrite(eastGreen, LOW);
      digitalWrite(eastYellow, HIGH);
      delay(changeDelay);
      
      digitalWrite(eastYellow, LOW);
      digitalWrite(eastRed, HIGH);
      delay(changeDelay);
      
      for ( int a = 0; a< 5; a++ )
      {
        digitalWrite(westYellow, LOW);
        delay(changeDelay);
        digitalWrite(westYellow, HIGH);
        delay(yellowBlinkTime);
      }
      
        digitalWrite(westYellow, LOW);
        digitalWrite(westRed, LOW);
        digitalWrite(westGreen, HIGH);
    }
  }
  
    if ( digitalRead(eastButton) == HIGH)
    {
      if ( trafficWest == true)
      {
        trafficWest = false;
        delay(flowTime);
        digitalWrite(westGreen, LOW);
        digitalWrite(westYellow, HIGH);
        delay(changeDelay);
        
        digitalWrite(westYellow, LOW);
        digitalWrite(westRed, HIGH);
        delay(changeDelay);
        
        for ( int a = 0; a < 5; a++ )
       {
          digitalWrite(eastYellow, LOW);
          delay(yellowBlinkTime);
          digitalWrite(eastYellow, HIGH);
          delay(yellowBlinkTime);
       }
        digitalWrite(eastYellow, LOW);
        digitalWrite(eastRed, LOW);
        digitalWrite(eastGreen, HIGH);
        }
       }
      }
     

